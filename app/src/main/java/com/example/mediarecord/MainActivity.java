package com.example.mediarecord;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化ButterKnife
        ButterKnife.bind(this);
        //动态获取相机、录音权限
        requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA},1000);
    }

    /**
     * 多个点击事件
     */
    @OnClick({R.id.tv_go,R.id.tv_play})
    public void onViewClicked(View view) {
        switch (view.getId()){
            case R.id.tv_go:
                Intent intent = new Intent(MainActivity.this, MediaRecordActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_play:
                Intent intent1 = new Intent(MainActivity.this, MediaPlayerActivity.class);
                startActivity(intent1);
                break;
            default:
                break;
        }
    }

}