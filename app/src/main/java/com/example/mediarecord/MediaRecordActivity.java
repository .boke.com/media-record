package com.example.mediarecord;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MediaRecordActivity extends AppCompatActivity {
    @BindView(R.id.ture_view)
    public TextureView mTextureView;
    @BindView(R.id.btn_start)
    public Button mButton;

    private boolean isStart = false; //默认false

    private MediaRecorder mediaRecorder;
    private Camera camera;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medio_record);
        //初始化ButterKnife
        ButterKnife.bind(this);
    }


    /**
     * 多个点击事件
     */
    @OnClick({R.id.btn_start})
    public void onViewClicked(View view) {
        switch (view.getId()){
            case R.id.btn_start:
                if(!isStart){
                    mButton.setText("结束");
                    isStart = true;
                    startMediaRecord();
                }else {
                    mButton.setText("开始");
                    isStart = false;
                    stopMediaRecord();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 开始录制
     */
    private void startMediaRecord(){
        //设置摄像头角度 使拍摄时方向为竖直
        camera = Camera.open();
        camera.setDisplayOrientation(90);
        camera.unlock();
        mediaRecorder = new MediaRecorder();
        //设置摄像头
        mediaRecorder.setCamera(camera);
        //设置音频源 麦克风
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        //设置视频源
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        //指定视频文件格式MP4
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //设置音频编码
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        //设置视频编码
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        //设置输出角度 使本地录制的视频方向竖直
        mediaRecorder.setOrientationHint(90);
        //设置视频输出文件
        mediaRecorder.setOutputFile(new File(getExternalFilesDir(""),"a.mp4").getAbsolutePath());
        //设置视频大小
        mediaRecorder.setVideoSize(640,480);
        //设置摄像头预览画布
        mediaRecorder.setPreviewDisplay(new Surface(mTextureView.getSurfaceTexture()));
        //
        try {
            mediaRecorder.prepare();

        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaRecorder.start();
    }

    /**
     * 结束录制
     */
    private void stopMediaRecord(){
        mediaRecorder.stop();
        mediaRecorder.release();
        camera.stopPreview();
        camera.release();
    }

}